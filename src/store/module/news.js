export default {
    state : {
        news : [],
    },
    getters : {

    },
    mutations : {
        setNews(state,data){
            state.news = data;
        }
    },
    actions : {
        async getNews({
            commit
        }) {
            let response = await axios.get('/news').then((res) => {
                commit('setNews', res.data);
            })
        },
    }
}