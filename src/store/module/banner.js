export default {
    state: {
        bannerHome : [],
        bannerMid : [],
        bannerReccomeded : [],
        bannerTop : []
    },
    getters: {

    },
    mutations: {
        async setBanner(state, data) {
            state.bannerHome = await data.filter((q)=>q.bh_type == 1);
            state.bannerMid = await data.filter((q)=>q.bh_type == 2);
            state.bannerReccomeded = await data.filter((q)=>q.bh_type == 3);
            state.bannerTop = await data.filter((q)=>q.bh_type == 4);
        },
        updateBannerHome(state,value){
            state.bannerHome = value;
            updateIndext(1,value);
        },
        updateBannerMid(state,value){
            state.bannerMid = value;
            updateIndext(2,value);
        },
        updateBannerReccomeded(state,value){
            state.bannerReccomeded = value;
            updateIndext(3,value);
        },
        updateBannerTop(state,value){
            state.bannerReccomeded = value;
            updateIndext(4,value);
        }
    },
    actions: {
        async getBanner({
            commit
        }) {
            let response = await axios.get('/banner').then((res) => {
                commit('setBanner', res.data);
            })
        },
    }
}

function updateIndext(type, value) {
    axios.put('bannerUpdateIndex/' + type,{
        params : JSON.stringify(value)}).then((res) => {

    }).catch((err) => {

    }).then(()=>{
        
    })
}