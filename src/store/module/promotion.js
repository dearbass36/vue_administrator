export default {
    state : {
        flashsale : [],
        coupon : []
    },
    getters : {
        flashsaleRun(state){
            return state.flashsale.find((q)=>q.fs_status==1);
        },
        couponByID : (state) => (id) => {
            return state.coupon.find((q)=>q.id == id);
        }
    },
    mutations : {
        setFlashsale(state,data){
            state.flashsale = data;
        },
        setCoupon(state,data){
            state.coupon = data;
        }
    },
    actions : {
        async getFlashsale({
            commit
        }) {
            let response = await axios.get('/flashsale').then((res) => {
                commit('setFlashsale', res.data);
            })
        },
        async getCoupon({
            commit
        }) {
            let response = await axios.get('/coupon').then((res) =>{
                commit('setCoupon', res.data);
            });
        }
    }
}