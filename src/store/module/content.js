export default {
    state: {
        content: [],
    },
    getters: {
        contentByName : (state) => (name) => {
            return state.content.find((q)=>q.content_name === name);
        }
    },
    mutations: {
        setContent(state, data) {
            state.content = data;
        }
    },
    actions: {
        async getContent({
            commit
        }) {
            let response = await axios.get('/content').then((res) => {
                commit('setContent', res.data);
            })
        },
    }
}