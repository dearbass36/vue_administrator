export default {
    state: {
        inbox: []
    },
    getters: {
        InboxbyID : (state) => (id) => {
            return state.inbox.find( (q) => q.id == id );
        }
    },
    mutations: {
        setInbox(state, data) {
            state.inbox = _.orderBy(data,['created_at'],['desc']);
        }
    },
    actions: {
        async getInbox({
            commit
        }) {
            let response = await axios.get('/mailboxs').then((res) => {
                commit('setInbox', res.data);
            })
        }
    }
}