import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);
import moduleProduct from "./module/product";
import modulePromotion from "./module/promotion";
import moduleNews from "./module/news"
import moduleContent from "./module/content"
import moduleBanner from "./module/banner"
import moduleInbox from "./module/inbox"
export default new Vuex.Store({
    modules: {
      moduleProduct: moduleProduct,
      modulePromotion: modulePromotion,
      moduleNews : moduleNews,
      moduleContent : moduleContent,
      moduleBanner : moduleBanner,
      moduleInbox : moduleInbox
    }
  })