export default [{
    path: '/404',
    redirect: '/Home'
  },
  {
    path: "/",
    redirect: '/login',
    meta: {
      auth: false
    }
  },
  {
    path: "/Home",
    component: () => import(
      `@/views/Home.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    name: "/Product",
    path: "/Product/:id",
    component: () => import(
      `@/views/Product/viewProduct.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    name: "/EditProduct",
    path: "/EditProduct/:id",
    component: () => import(
      `@/views/Product/editProduct.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/NewProduct",
    component: () => import(
      `@/views/Product/newProduct.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/Category",
    component: () => import(
      `@/views/Product/Category.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/Brand",
    component: () => import(
      `@/views/Product/Brand.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/Order",
    component: () => import(
      `@/views/Order/viewOrder.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/Promotion",
    component: () => import(
      `@/views/Discount/Promotion.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/Coupon",
    component: () => import(
      `@/views/Discount/Coupon.vue`
    ),
    meta: {
      auth: true
    },
  },
  {
    path: "/NewCoupon",
    component: () => import(
      `@/views/Discount/newCoupon.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    name : "/EditCoupon",
    path: "/EditCoupon/:id",
    component: () => import(
      `@/views/Discount/editCoupon.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/Flashsale",
    component: () => import(
      `@/views/Discount/Flashsale.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/NewFlashsale",
    component: () => import(
      `@/views/Discount/newFlashsale.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/PageManage/Home",
    component: () => import(
      `@/views/PageManage/Home.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/PageManage/About",
    component: () => import(
      `@/views/PageManage/About.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    name: "/PageManage/Service",
    path : "/PageManage/Service/:content",
    component: () => import(
      `@/views/PageManage/Service.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/News",
    component: () => import(
      `@/views/News.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/Inbox",
    component: () => import(
      `@/views/MailBox/Inbox.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    name: "/Inbox",
    path : "/Inbox/:id",
    component: () => import(
      `@/views/MailBox/Message.vue`
    ),
    meta: {
      auth: true
    }
  },
  {
    path: "/login",
    component: () => import(
      `@/views/Login.vue`
    ),
    meta: {
      auth: false
    }
  }
];