import Vue from 'vue';
import Router from 'vue-router';
import paths from './path.js';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

Vue.use(Router);
const router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    linkActiveClass: 'active',
    routes: paths
});
// router gards
router.beforeEach((to, from, next) => {
    NProgress.start();
    next();
});

router.afterEach((to, from) => {
    NProgress.done();
});

export default router;