import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router/'
import store from './store/'
import axios from 'axios';
import VueAxios from 'vue-axios';
import VeeValidate from 'vee-validate';
import _ from 'lodash';
import VueSweetalert2 from 'vue-sweetalert2';
import Editor from '@tinymce/tinymce-vue';
import CatMenu from "./components/CatMenu.vue"
import './stylus/main.styl'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import i18n from './i18n'
import FlagIcon from 'vue-flag-icon'
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css' 

Vue.use(FlagIcon);
Vue.component('cat-menu',CatMenu);
Vue.component('editor-tiny',Editor);
Vue.component('vueDropzone',vue2Dropzone);
Vue.use(VueSweetalert2);
Vue.use(_);
Vue.use(VueAxios, axios);
Vue.use(VeeValidate);
Vue.use(VueMoment, {
  moment,
})
window.axios = require('axios');
axios.defaults.baseURL = process.env.VUE_APP_ROOT_API+'/api';
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
const token = localStorage.getItem('default_auth_token');
Vue.router = router
Vue.use(require('@websanova/vue-auth'), {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
});

Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
