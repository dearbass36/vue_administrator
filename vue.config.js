module.exports = {
  pluginOptions: {
    i18n: {
      locale: 'th',
      fallbackLocale: 'en',
      localeDir: 'locals',
      enableInSFC: true
    }
  }
}
